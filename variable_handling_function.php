<?php

$myVar= "354.25 Hello World";
$result=floatval($myVar);
echo $result."<br>";

$myVar= "Hello 354.25 World";
$result=floatval($myVar);
echo $result."<br>";

$myVar= "Hello World 354.25";
$result=floatval($myVar);
echo $result."<br>";

$myVar= "Hello World";
$result=floatval($myVar);
echo $result."<br>";




$myVar= "560.24";
if(empty($myVar)){
    echo "$myVar is empty <br>";
    }
else{
    echo "$myVar is not empty";
}


$myVar= 0.0001;
if(empty($myVar)){
    echo "$myVar is empty <br>";
}
else{
    echo "$myVar is not empty";
}


$myVar= -0.01;
if(empty($myVar)){
    echo "$myVar is empty <br>";
}
else{
    echo "$myVar is not empty"."<br>"."<br>";
}


$myVar= array("Quddus","Moyna", "Jorina", "Riva","Sayma");
$serializedString = serialize($myVar);
    echo $serializedString."<br>";

$myArray=unserialize($serializedString);

echo "<pre>";

print_r($myArray);



//set & unset, var_dump example

var_dump($myArray);
unset($myArray);
if (isset($myArray)){
    echo "This variable is set";
}
else{
    echo "This variable is not set";
}


//var_export example

echo "<br>";
var_export($myArray);
   echo "<br>";


//boolval example

$result = boolval("ffgfgrgr");
var_dump($result); //true;

$result = boolval("0");
var_dump($result); //false;

$result = boolval(0);
var_dump($result); //false;

$result = boolval(0.0000);
var_dump($result); //false;

$result = boolval(0.0001);
var_dump($result); //true;

$result = boolval(-0.0001);
var_dump($result); //true;