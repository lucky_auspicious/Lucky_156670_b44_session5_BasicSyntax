<?php

//Boolean example started here

$decision = true;
if($decision) echo "This decision is true<br>";
$decision = false;
if($decision) echo "This decision is false";



//integer example started here

$value = 10;
echo $value."<br>";


//Float/Double example started here

$value = 20;
echo $value."<br>";



//string example started here

$var = 50;
$string1 = 'This is a single quoted string $var <br>';
$string2 = "This is a double quoted string $var <br>";
echo $string1;
echo $string2;
echo "<br>";


$heredocString=<<< BITM
This is a heredoc example line1 $var<br>
This is a heredoc example line2 $var<br>
This is a heredoc example line3 $var<br>
BITM;

$nowdocString=<<<'BITM'
This is a nowdoc example line1 $var<br>
This is a nowdoc example line2 $var<br>
This is a nowdoc example line3 $var<br>
BITM;

echo $heredocString."<br><br>".$nowdocString;
echo "<br>";

//array example started here

$arr=array(1,2,3,4,5);
print_r($arr);
echo "<br>";

$arr=array("BMW","Toyota","NISSAN","Ferrari","Maruti");
print_r($arr);
echo "<br>";
echo "<br>";

$ageArray = array("Arif"=>30,"Moynar Maa"=>45,"Shaila"=>85);
print_r($ageArray);
echo "<br>";

echo "The_age_of_Moynar_Ma is ".$ageArray["Moynar Maa"];
echo "<br>";

?>



