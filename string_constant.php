<?php
echo "How are \"you\" ";
echo "<br>";

$myStr = addslashes('Hello "37" Hello \37\ Hello \'37\' ');
        echo $myStr;
echo "<br>";


//exlpode example

$myStr = "Hello World! How is life?";
echo "<br>";
$myArr = explode(" ", $myStr);
print_r($myArr);

echo "<br>";
echo "<br>";
echo "<br>";



//imlpode example

$myStr = implode(" * ", $myArr);
echo $myStr."<br>";

$myStr = implode(" * # ", $myArr);
echo $myStr;

echo "<br>";
echo "<br>";

//To show html code in output

$myStr = "<br> means line break";
echo $myStr."<br>";

$myStr = "<br> means line break";
echo htmlentities($myStr);

echo "<br>";

//OR

$myStr = "<br> means line break";
$myStr = htmlentities($myStr);
echo $myStr;

echo "<br>";

//trim example

$myStr = "   Hello World   ";
echo trim($myStr)."<br>";

$myStr = "   Hello World   ";
echo rtrim($myStr)."<br>";

$myStr = "   Hello World   ";
echo ltrim($myStr)."<br>";

//new line to br

$myStr = "\n\n\n\nHi There!";
echo nl2br($myStr);

echo "<br> <br>";

//str_pad

$mainStr = "Hello World";
$padStr = str_pad($mainStr, 50, "*");
    echo $padStr;

echo "<br> <br>";

$mainStr = "Hello World";
$padStr = str_pad($mainStr, 48, "$mainStr");
echo $padStr;

echo "<br> <br>";

//str_repeat

$mainStr = "Hello World ";
$repeatedStr = str_repeat($mainStr, 7);
echo $repeatedStr;

echo "<br> <br>";


//str_replace

$mainStr = "Hello World";
$replacedStr = str_replace("o","O",$mainStr);
echo $replacedStr;

echo "<br> <br>";

//str_split

$mainStr = "Hello World";
$myArr = str_split($mainStr);
print_r($myArr);

echo "<br> <br>";



//strlen

$mainStr = "Hello World";
$myArr = strlen($mainStr);
echo $myArr;

echo "<br> <br>";



//strtolower

$mainStr = "Hello World";
$myArr = strtolower($mainStr);
echo $myArr;

echo "<br> <br>";


//strtoupper

$mainStr = "Hello World";
$myArr = strtoupper($mainStr);
echo $myArr;

echo "<br> <br>";


//substr_compare

$mainStr = "Hello World";
$subStr = "Hello";
$myArr = substr_compare($mainStr,$subStr,0);
echo $myArr;

echo "<br> <br>";


//substr_count

$mainStr = "Hello World Hello Friends Hello World";
$subStr = "World";
$myArr = substr_count($mainStr,$subStr);
echo $myArr;

echo "<br> <br>";


//substr_replace

$mainStr = "Hello World Hello Friends";
$subStr = "MyWorld";
$myArr = substr_replace($mainStr,$subStr,8);
echo $myArr;

echo "<br> <br>";


//ucfirst

$mainStr = "hello world hello friends";
$myArr = ucfirst($mainStr);
echo $myArr;

echo "<br> <br>";






